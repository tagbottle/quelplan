import firebase from "firebase/app";
import { GetterTree, ActionTree, MutationTree } from "vuex";

import { IAuthState, IState } from "./index";

type ModuleState = IAuthState;

/*
 * state
 */

export const state = (): ModuleState => ({
  initialized: false,
  loggedIn: false
});

/*
 * getters
 */

export const getters: GetterTree<ModuleState, IState> = {
  initialized: state => state.initialized,
  loggedIn: state => state.loggedIn
};

/*
 * actions
 */

export const actions: ActionTree<ModuleState, IState> = {
  async login({ state, commit }) {
    const provider = new this.$firebase.auth.GoogleAuthProvider();
    const result = await this.$firebase.auth().signInWithPopup(provider);

    commit("INITIALIZED");
    if (result) {
      commit("LOGIN");
    } else {
      commit("LOGOUT");
    }
  },

  async logout({ state, dispatch, commit }) {
    await this.$firebase.auth().signOut();
    commit("LOGOUT");
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<ModuleState> = {
  INITIALIZED(state) {
    state.initialized = true;
  },
  LOGIN(state) {
    state.loggedIn = true;
  },
  LOGOUT(state) {
    state.loggedIn = false;
  }
};
