export interface IState {
  auth: IAuthState;
  uploader: IUploaderState;
  searcher: ISearcherState;
}

export interface IAuthState {
  initialized: boolean;
  loggedIn: boolean;
}

export interface IUploaderState {
  uploading: boolean;
  tags: string[];
  files: File[];
}

export interface ISearcherState {
  searching: boolean;
  tags: string[];
  imageUrls: string[];
}
