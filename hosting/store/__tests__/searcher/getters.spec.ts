import { getters } from "~/store/searcher";
import {
  defaultState,
  defaultSearcherState
} from "~/__mocks__/util/defaultState";
import { ISearcherState, IState } from "~/store";

describe("seacher getters", () => {
  let state: ISearcherState;
  let rootState: IState;

  beforeEach(() => {
    state = defaultSearcherState();
    rootState = defaultState();
  });

  it("has `searching` getter", () => {
    // on
    state.searching = true;
    expect(getters["searching"](state, null, rootState, null)).toBe(true);

    // off
    state.searching = false;
    expect(getters["searching"](state, null, rootState, null)).toBe(false);
  });

  it("has `tags` getter", () => {
    // set values
    state.tags = ["a", "b", "c"];

    // get
    expect(getters["tags"](state, null, rootState, null)).toEqual([
      "a",
      "b",
      "c"
    ]);
  });

  it("has `imageUrls` getter", () => {
    // set values
    state.imageUrls = [
      "https://test.url/1",
      "https://test.url/2",
      "https://test.url/3"
    ];

    // get
    expect(getters["imageUrls"](state, null, rootState, null)).toEqual([
      "https://test.url/1",
      "https://test.url/2",
      "https://test.url/3"
    ]);
  });
});
