import { ISearcherState } from "~/store";

import { mutations, state as defaultState } from "~/store/searcher";

describe("searcher mutations", () => {
  let state: ISearcherState;

  beforeEach(() => {
    state = defaultState();
  });

  it("has SAVE_TAGS mutation", () => {
    const tags = ["a", "b", "c"];
    mutations["SAVE_TAGS"](state, { tags });
    expect(state.tags).toEqual(tags);
  });

  it("has REMOVE_IMAGE_URLS mutation", () => {
    // initial state
    state.imageUrls = ["https://test.url/1", "https://test.url/2"];

    // remove all
    mutations["REMOVE_IMAGE_URLS"](state, null);
    expect(state.imageUrls).toEqual([]);
  });

  it("has ADD_IMAGE_URL mutation", () => {
    // initial state
    state.imageUrls = ["https://test.url/1", "https://test.url/2"];

    // add new url
    const imageUrl = "https://test.url/3";
    mutations["ADD_IMAGE_URL"](state, { imageUrl });
    expect(state.imageUrls).toEqual([
      "https://test.url/1",
      "https://test.url/2",
      "https://test.url/3"
    ]);
  });

  it("has SEARCHING mutation", () => {
    // initial state
    state.searching = false;

    // on
    mutations["ADD_IMAGE_URL"](state, { searching: true });
    state.searching = true;

    // off
    mutations["ADD_IMAGE_URL"](state, { searching: false });
    state.searching = false;
  });
});
