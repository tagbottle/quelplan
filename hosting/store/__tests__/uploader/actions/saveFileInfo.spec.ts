import firebase from "firebase/app";
import MockFirestore from "firebase-mock/src/firestore";
import MockFirestoreDocument from "firebase-mock/src/firestore-document";

import { actions } from "~/store/uploader";
import { login } from "~/__mocks__/util/firebaseAuth";
const { saveFileInfo } = actions;

describe("uploader actions: saveFileInfo", () => {
  const context = {};
  let props: { name: string; path: string; tags: string[] };
  let firestoreMock: MockFirestore;
  let bindFunction: (_, { name, path, tags }) => Promise<MockFirestoreDocument>;

  beforeEach(() => {
    // mock for auth
    login();
    bindFunction = saveFileInfo.bind({ $firebase: firebase });

    // mock for firestore
    firestoreMock = firebase.firestore() as MockFirestore;

    // generate props
    props = {
      name: "test file.jpg",
      path: "path/in/storage",
      tags: ["a", "b", "c"]
    };
  });

  it("returns the doc ref with correct path", async () => {
    // call action
    const test = bindFunction({}, props).then(result => {
      expect(result.path).toMatch(/^files\/users\/testUid\/.+$/);
    });

    // flush
    setTimeout(() => firestoreMock.flush(), 100);

    // return test
    return test;
  });

  it("saves props in doc", async () => {
    // call action
    const test = bindFunction({}, props).then(result => {
      expect(result.data).toHaveProperty("name", "test file.jpg");
      expect(result.data).toHaveProperty("path", "path/in/storage");
      expect(result.data).toHaveProperty("tags", { a: true, b: true, c: true });
      expect(result.data).toHaveProperty("createdAt", {
        type: "serverTimestamp"
      });
      expect(result.data).toHaveProperty("lastUpdatedAt", {
        type: "serverTimestamp"
      });
    });

    // flush
    setTimeout(() => firestoreMock.flush(), 100);

    // return test
    return test;
  });
});
