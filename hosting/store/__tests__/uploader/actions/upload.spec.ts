import firebase from "firebase/app";
import { ActionContext, Dispatch, Commit } from "vuex";
import * as either from "fp-ts/lib/Either";
import MockStorageReference from "firebase-mock/src/storage-reference";

import { IState, IUploaderState } from "~/store";
import { actions } from "~/store/uploader";
import { defaultState } from "~/__mocks__/util/defaultState";
import { login, logout } from "~/__mocks__/util/firebaseAuth";

const { upload } = actions;

describe("uploader actions: upload", () => {
  let bindFunction: ({ state, commit, dispatch }) => Promise<void>;
  let context: ActionContext<IUploaderState, IState>;

  const getContext = (): ActionContext<IUploaderState, IState> => {
    const rootState = defaultState();
    const state = rootState.uploader;
    const commit: Commit = jest.fn();
    const dispatch: Dispatch = jest.fn((...args) => {
      const types: string = args[0];
      if (types == "uploadFilesToCloudStorage") {
        const ref1 = new MockStorageReference(
          "mock-storage",
          null,
          "file/path/ref1"
        );
        const ref2 = new MockStorageReference(
          "mock-storage",
          null,
          "file/path/ref2"
        );
        const ref3 = new MockStorageReference(
          "mock-storage",
          null,
          "file/path/ref3"
        );

        return either.right([ref1, ref2, ref3]);
      }
    });
    const getters = {};
    const rootGetters = {};

    // overwrite default state
    state.uploading = false;
    state.tags = ["a", "b", "c"];
    state.files = [
      new File([], "test file 1"),
      new File([], "test file 2"),
      new File([], "test file 3")
    ];

    return {
      state,
      rootState,
      commit,
      dispatch,
      getters,
      rootGetters
    };
  };

  beforeEach(() => {
    // mock for auth
    login();
    bindFunction = upload.bind({ $firebase: firebase });

    // set context
    context = getContext();
  });

  it("fails if state.uploading == true", async () => {
    context.state.uploading = true;
    try {
      await bindFunction(context);
    } catch (e) {
      expect(e).toEqual(new Error("アップロード中"));
    }
  });

  it("fails if auth info is blank", async () => {
    // logout in mock
    logout();
    try {
      await bindFunction(context);
    } catch (e) {
      expect(e).toEqual(new Error("ログインしてください :("));
    }
  });

  it("fails if state.tags is empty array", async () => {
    context.state.tags = [];
    try {
      await bindFunction(context);
    } catch (e) {
      expect(e).toEqual(new Error("タグが指定されてないよ"));
    }
  });

  it("fails if state.tags is empty array", async () => {
    context.state.files = [];
    try {
      await bindFunction(context);
    } catch (e) {
      expect(e).toEqual(new Error("アップロードするファイルを指定してないよ"));
    }
  });

  it("uses uploading flag while working", async () => {
    await bindFunction(context);
    expect(context.commit).toBeCalledWith("UPLOADING", { uploading: false });
    expect(context.commit).toBeCalledWith("UPLOADING", { uploading: true });
  });

  it("dispatches upload actions", async () => {
    await bindFunction(context);
    expect(context.dispatch).toBeCalledWith("uploadFilesToCloudStorage");
  });

  it("dispatches creation actions of tag docs", async () => {
    await bindFunction(context);
    expect(context.dispatch).toBeCalledWith("saveTagInfo", {
      tag: context.state.tags[0]
    });
    expect(context.dispatch).toBeCalledWith("saveTagInfo", {
      tag: context.state.tags[1]
    });
    expect(context.dispatch).toBeCalledWith("saveTagInfo", {
      tag: context.state.tags[2]
    });
  });

  it("dispatches creation actions of tag docs", async () => {
    await bindFunction(context);
    expect(context.dispatch).toBeCalledWith("saveFileInfo", {
      name: context.state.files[0].name,
      path: "file/path/ref1",
      tags: context.state.tags
    });
    expect(context.dispatch).toBeCalledWith("saveFileInfo", {
      name: context.state.files[1].name,
      path: "file/path/ref2",
      tags: context.state.tags
    });
    expect(context.dispatch).toBeCalledWith("saveFileInfo", {
      name: context.state.files[2].name,
      path: "file/path/ref3",
      tags: context.state.tags
    });
  });

  it("dispatches upload actions", async () => {
    await bindFunction(context);
    expect(context.dispatch).toBeCalledWith("uploadFilesToCloudStorage");
  });

  it("reset uploading flag even if error happens while uploading", async () => {
    context.dispatch = async () => {
      return either.left(new Error("some upload error"));
    };

    try {
      console.error = jest.fn();
      await bindFunction(context);
    } catch (e) {
      expect(e).toEqual(new Error("アップロード失敗 :("));
    }

    expect(context.state.uploading).toBe(false);
  });
});
