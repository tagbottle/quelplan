import firebase from "firebase/app";
import { Dispatch } from "vuex";
import MockStorageReference from "firebase-mock/src/storage-reference";

import { actions } from "~/store/uploader";
import { login } from "~/__mocks__/util/firebaseAuth";
import { IState, IUploaderState } from "~/store";
import { defaultUploaderState } from "~/__mocks__/util/defaultState";
import { Either, Right } from "fp-ts/lib/Either";
import { file } from "babel-types";

const { uploadFilesToCloudStorage } = actions;

describe("uploader actions: uploadFilesToCloudStorage", () => {
  let context: { dispatch: Dispatch; state: IUploaderState };
  let bindFunction: (context) => Promise<MockStorageReference>;

  const getContext = (): { dispatch: Dispatch; state: IUploaderState } => {
    // dispatch mock
    const dispatch: Dispatch = jest.fn(
      (type: string, props: { id: string; file: File }) => {
        if (
          type == "uploadOneFileToCloudStorage" &&
          !!props.id &&
          !!props.file
        ) {
          return new MockStorageReference(
            "mock-storage",
            null,
            `file/path/${props.id}`
          );
        }

        throw `dispatch mock error: unexpected type ${type}`;
      }
    );

    // dummy state with 3 files to upload
    const state = defaultUploaderState();
    state.files = [
      new File([], "dummy file 1"),
      new File([], "dummy file 2"),
      new File([], "dummy file 3")
    ];

    return { dispatch, state };
  };

  beforeEach(() => {
    // mock for auth
    login();
    bindFunction = uploadFilesToCloudStorage.bind({ $firebase: firebase });

    // set context
    context = getContext();
  });

  it("returns a right instance", async () => {
    // call action
    const result: Either<
      Error,
      firebase.storage.Reference[]
    > = await bindFunction(context);

    // check result
    expect(result).toBeInstanceOf(Right);
  });

  it("calls `uploadOneFileToCloudStorage` action with correct arguments", async () => {
    // call action
    const result: Either<
      Error,
      firebase.storage.Reference[]
    > = await bindFunction(context);
    if (result.isLeft()) {
      expect(result).toBeInstanceOf(Right);
      return;
    }

    // check dispatch mock
    expect(context.dispatch).toBeCalledWith("uploadOneFileToCloudStorage", {
      id: expect.any(String),
      file: context.state.files[0]
    });
    expect(context.dispatch).toBeCalledWith("uploadOneFileToCloudStorage", {
      id: expect.any(String),
      file: context.state.files[1]
    });
    expect(context.dispatch).toBeCalledWith("uploadOneFileToCloudStorage", {
      id: expect.any(String),
      file: context.state.files[2]
    });
  });

  it("returns the results of `uploadOneFileToCloudStorage`", async () => {
    // call action
    const result: Either<
      Error,
      firebase.storage.Reference[]
    > = await bindFunction(context);
    if (result.isLeft()) {
      expect(result).toBeInstanceOf(Right);
      return;
    }

    // check result
    const references: firebase.storage.Reference[] = result.value;
    expect(references.length).toBe(3);
    for (const reference of references) {
      expect(reference.fullPath).toMatch(/file\/path\/.+/);
    }
  });
});
