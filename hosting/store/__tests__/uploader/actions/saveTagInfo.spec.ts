import firebase from "firebase/app";
import MockFirestore from "firebase-mock/src/firestore";
import MockFirestoreDocument from "firebase-mock/src/firestore-document";

import { actions } from "~/store/uploader";
import { login } from "~/__mocks__/util/firebaseAuth";
const { saveTagInfo } = actions;

describe("uploader actions: saveTagInfo", () => {
  const context = {};
  let props: { tag: string };
  let firestoreMock: MockFirestore;
  let bindFunction: (_, { tag }) => Promise<MockFirestoreDocument>;

  beforeEach(() => {
    // mock for auth
    login();
    bindFunction = saveTagInfo.bind({ $firebase: firebase });

    // mock for firestore
    firestoreMock = firebase.firestore() as MockFirestore;

    // generate props
    props = {
      tag: "a"
    };
  });

  it("returns the doc ref with correct path", async () => {
    // call action
    const test = bindFunction({}, props).then(result => {
      expect(result.path).toEqual(`tags/users/testUid/a`);
    });

    // flush
    setTimeout(() => firestoreMock.flush(), 100);
    setTimeout(() => firestoreMock.flush(), 200);

    // return test
    return test;
  });

  it("saves props in doc", async () => {
    // call action
    const test = bindFunction({}, props).then(result => {
      expect(result.data).toHaveProperty("value", "a");
      expect(result.data).toHaveProperty("alias", ["a"]);
      expect(result.data).toHaveProperty("createdAt", {
        type: "serverTimestamp"
      });
      expect(result.data).toHaveProperty("lastUpdatedAt", {
        type: "serverTimestamp"
      });
    });

    // flush
    setTimeout(() => firestoreMock.flush(), 100);

    // return test
    return test;
  });
});
