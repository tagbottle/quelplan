import firebase from "firebase/app";
import MockStorageReference from "firebase-mock/src/storage-reference";

import { actions } from "~/store/uploader";
import { login } from "~/__mocks__/util/firebaseAuth";

const { uploadOneFileToCloudStorage } = actions;

describe("uploader actions: uploadOneFileToCloudStorage", () => {
  const context = {};
  let props: { id: string; file: File };

  let bindFunction: (_, { id, file }) => Promise<MockStorageReference>;

  beforeEach(() => {
    // mock for auth
    login();
    bindFunction = uploadOneFileToCloudStorage.bind({ $firebase: firebase });

    // generate props
    props = {
      id: "test-uuid",
      file: new File([], "test file")
    };
  });

  it("returns the file path object of image", async () => {
    // call action
    const result = await bindFunction({}, props);

    // check file
    expect(result).toBeInstanceOf(MockStorageReference);
    expect(result.fullPath).toEqual("img/testUid/test-uuid/raw");
  });

  it("saves image file in cloud storage", async () => {
    // call action
    await bindFunction({}, props);

    // check file
    const ref: MockStorageReference = firebase
      .storage()
      .ref("img/testUid/test-uuid/raw");
    expect(ref._contents).not.toBeNull();
  });
});
