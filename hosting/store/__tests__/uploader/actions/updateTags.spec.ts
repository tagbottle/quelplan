import { actions, state as defaultState } from "~/store/uploader";
const { updateTags } = actions;

describe("uploader actions: updateTags", () => {
  it("commits new tags", () => {
    const commit = jest.fn();
    const tags = ["a", "b", "c"];
    updateTags({ commit }, { tags });
    expect(commit).toBeCalled();
    expect(commit.mock.calls[0][0]).toEqual("SAVE_TAGS");
    expect(commit.mock.calls[0][1]).toEqual({ tags });
  });
});
