import { actions, state as defaultState } from "~/store/uploader";
const { updateFiles } = actions;

describe("uploader actions: updateTags", () => {
  it("commits new tags", () => {
    const commit = jest.fn();
    const files = [new File([], "test file")];
    updateFiles({ commit }, { files });
    expect(commit).toBeCalled();
    expect(commit.mock.calls[0][0]).toEqual("SAVE_FILES");
    expect(commit.mock.calls[0][1]).toEqual({ files });
  });
});
