import { actions, state as defaultState } from "~/store/uploader";
const { removeFile } = actions;

describe("uploader actions: removeFile", () => {
  it("commits removal tag index", () => {
    const commit = jest.fn();
    const index = 10;
    removeFile({ commit }, { index });
    expect(commit).toBeCalled();
    expect(commit.mock.calls[0][0]).toEqual("REMOVE_FILE");
    expect(commit.mock.calls[0][1]).toEqual({ index });
  });
});
