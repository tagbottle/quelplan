import { mutations, state as defaultState } from "~/store/uploader";

describe("uploader mutations", () => {
  it("SAVE_TAGS", () => {
    const state = defaultState();
    const tags = ["a", "b", "c"];
    mutations["SAVE_TAGS"](state, { tags });
    expect(state.tags).toEqual(tags);
  });

  it("SAVE_FILES", () => {
    const state = defaultState();
    const files = [new File([], "test file")];
    mutations["SAVE_FILES"](state, { files });
    expect(state.files).toEqual(files);
  });

  it("REMOVE_FILE", () => {
    // 1. add file
    const state = defaultState();
    const file1 = new File([], "test file 1");
    const file2 = new File([], "test file 2");
    const file3 = new File([], "test file 3");
    const files = [file1, file2, file3];
    mutations["SAVE_FILES"](state, { files });
    expect(state.files).toEqual(files);

    // 2. remove it
    mutations["REMOVE_FILE"](state, { index: 1 });
    expect(state.files).toEqual([file1, file3]);
  });

  it("UPLOADING", () => {
    const state = defaultState();

    // on
    mutations["UPLOADING"](state, { uploading: true });
    expect(state.uploading).toEqual(true);

    // off
    mutations["UPLOADING"](state, { uploading: false });
    expect(state.uploading).toEqual(false);
  });
});
