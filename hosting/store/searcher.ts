import firebase from "firebase/app";
import Vue from "vue";
import { GetterTree, ActionTree, MutationTree } from "vuex";

import { IState, ISearcherState } from "./index";

type ModuleState = ISearcherState;

/*
 * state
 */

export const state = (): ModuleState => ({
  tags: [],
  imageUrls: [],
  searching: false
});

/*
 * getters
 */

export const getters: GetterTree<ModuleState, IState> = {
  searching(state): boolean {
    return !!state.searching;
  },
  tags(state): string[] {
    return Object.assign([], state.tags);
  },
  imageUrls(state): string[] {
    return Object.assign([], state.imageUrls);
  }
};

/*
 * actions
 */

export interface IActions extends ActionTree<ModuleState, IState> {
  updateTags({ commit, state }, props: { tags: string[] });
}

export const actions: IActions = {
  async updateTags({ commit, state }, props) {
    commit("SAVE_TAGS", props);
    commit("REMOVE_IMAGE_URLS");

    if (!props.tags.length) {
      return;
    }

    const db = this.$firebase.firestore();
    const storage = this.$firebase.storage();
    let query: firebase.firestore.Query = db
      .collection("files")
      .doc("users")
      .collection(this.$firebase.auth().currentUser.uid)
      .limit(100);

    // search by tags
    for (const tag of state.tags) {
      query = query.where(`tags.${tag}`, "==", true);
    }

    const files = await query.get();
    files.docs.forEach(async fileDoc => {
      const filePath: string = fileDoc.data().path;
      const fileRef: firebase.storage.Reference = storage.ref(filePath);
      const imageUrl: string = await fileRef.getDownloadURL();
      commit("ADD_IMAGE_URL", { imageUrl });
    });
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<ModuleState> = {
  SAVE_TAGS(state, props: { tags: string[] }) {
    const tags = Object.assign([], props.tags);
    Vue.set(state, "tags", tags);
  },

  REMOVE_IMAGE_URLS(state) {
    Vue.set(state, "imageUrls", []);
  },

  ADD_IMAGE_URL(state, props: { imageUrl: string }) {
    state.imageUrls.push(props.imageUrl);
  },

  SEARCHING(state, props: { searching: boolean }) {
    state.searching = props.searching;
  }
};
