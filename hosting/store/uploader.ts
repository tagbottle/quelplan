import firebase from "firebase/app";
import Vue from "vue";
import { GetterTree, ActionTree, MutationTree } from "vuex";
import uuid from "uuid/v4";
import * as either from "fp-ts/lib/Either";

import { IState, IUploaderState } from "./index";

type ModuleState = IUploaderState;
type ActionResult<R> = either.Either<Error, R>;

/*
 * state
 */

export const state = (): ModuleState => ({
  tags: [],
  files: [],
  uploading: false
});

/*
 * getters
 */

export const getters: GetterTree<ModuleState, IState> = {
  uploading(state): boolean {
    return !!state.uploading;
  },
  tags(state): string[] {
    return Object.assign([], state.tags);
  },
  files(state): File[] {
    return Object.assign([], state.files);
  }
};

/*
 * actions
 */

export interface IActions extends ActionTree<ModuleState, IState> {
  /**
   * update tags value in store, which will be used by uploading logic
   * @param commit
   * @param props
   */
  updateTags({ commit }, props: { tags: string[] });

  /**
   * update files in store, which will be uploaded
   * @param commit
   * @param props
   */
  updateFiles({ commit }, props: { files: File[] });

  /**
   * remove specific file by its index
   * @param commit
   * @param props
   */
  removeFile({ commit }, props: { index: number });

  /**
   * upload files with requested tags
   * @param state
   * @param commit
   * @param dispatch
   */
  upload({ state, commit, dispatch }): Promise<void>;

  /*
   * [internal]  upload files to firebase storage
   */

  uploadFilesToCloudStorage({
    dispatch,
    state
  }): Promise<ActionResult<firebase.storage.Reference[]>>;
  uploadOneFileToCloudStorage(
    _,
    props: { id: string; file: File }
  ): Promise<firebase.storage.Reference>;

  /*
   * [internal] create|update info in firestore documents
   */

  saveTagInfo(
    _,
    props: { tag: string }
  ): Promise<firebase.firestore.DocumentReference>;
  saveFileInfo(
    _,
    props: { name: string; path: string; tags: string[] }
  ): Promise<firebase.firestore.DocumentReference>;
}

export const actions: IActions = {
  updateTags({ commit }, props) {
    commit("SAVE_TAGS", props);
  },

  updateFiles({ commit }, props) {
    commit("SAVE_FILES", props);
  },

  removeFile({ commit }, props: { index: number }) {
    commit("REMOVE_FILE", props);
  },

  async upload({ state, commit, dispatch }) {
    // check status
    if (state.uploading) {
      throw new Error("アップロード中");
    }
    if (!this.$firebase.auth().currentUser) {
      throw new Error("ログインしてください :(");
    }

    // validation
    if (!state.tags.length) {
      throw new Error("タグが指定されてないよ");
    }
    if (!state.files.length) {
      throw new Error("アップロードするファイルを指定してないよ");
    }

    // prepare for uploading
    commit("UPLOADING", { uploading: true });

    // upload files
    let fileUpdateResult: ActionResult<
      firebase.storage.Reference[]
    > = await dispatch("uploadFilesToCloudStorage");
    // error handling
    if (fileUpdateResult.isLeft()) {
      // reset uploading flag if uploading fails
      commit("UPLOADING", { uploading: false });

      // throw error
      throw new Error("アップロード失敗 :(");
    }

    // save tag info
    const tagSavingPromises = state.tags.map(
      async tag => await dispatch("saveTagInfo", { tag })
    );
    await Promise.all(tagSavingPromises);

    // save image file info
    const filePaths = fileUpdateResult.value;
    const fileSavingPromises = state.files.map(async (file, index) => {
      const name: string = file.name;
      const path = filePaths[index].fullPath;
      await dispatch("saveFileInfo", {
        name,
        path,
        tags: state.tags
      });
    });
    await Promise.all(fileSavingPromises);
    commit("REMOVE_ALL_FILES");
    commit("UPLOADING", { uploading: false });
  },

  async uploadFilesToCloudStorage({
    dispatch,
    state
  }): Promise<ActionResult<firebase.storage.Reference[]>> {
    // prepare for uploading
    const fileUploadingPromises: Promise<
      firebase.storage.Reference
    >[] = state.files.map(async (file: File) => {
      const id = uuid();
      return (await dispatch("uploadOneFileToCloudStorage", {
        id,
        file
      })) as firebase.storage.Reference;
    });

    // upload files
    try {
      const references = await Promise.all(fileUploadingPromises);
      return either.right(references);
    } catch (error) {
      return either.left(error);
    }
  },

  async uploadOneFileToCloudStorage(
    _,
    { id, file }
  ): Promise<firebase.storage.Reference> {
    // path for upload
    const path = `img/${this.$firebase.auth().currentUser.uid}/${id}/raw`;

    // generate upload params
    const fileRef: firebase.storage.Reference = this.$firebase
      .storage()
      .ref(path);
    await fileRef.put(file);

    // return the file ref
    return fileRef;
  },

  async saveTagInfo(
    _,
    props: { tag: string }
  ): Promise<firebase.firestore.DocumentReference> {
    // generate doc
    const db = this.$firebase.firestore();
    const tagRef: firebase.firestore.DocumentReference = db
      .collection("tags")
      .doc("users")
      .collection(this.$firebase.auth().currentUser.uid)
      .doc(props.tag);

    if ((await tagRef.get()).exists) {
      return tagRef;
    }

    await tagRef.set({
      value: props.tag,
      alias: [props.tag],
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      lastUpdatedAt: firebase.firestore.FieldValue.serverTimestamp()
    });

    return tagRef;
  },

  async saveFileInfo(
    _,
    props: { name: string; path: string; tags: string[] }
  ): Promise<firebase.firestore.DocumentReference> {
    // generate doc
    const db = this.$firebase.firestore();

    // create tags object
    const tags: { [key: string]: boolean } = {};
    for (const tag of props.tags) {
      tags[tag] = true;
    }

    return await db
      .collection("files")
      .doc("users")
      .collection(this.$firebase.auth().currentUser.uid)
      .add({
        name: props.name,
        path: props.path,
        tags,
        createdAt: firebase.firestore.FieldValue.serverTimestamp(),
        lastUpdatedAt: firebase.firestore.FieldValue.serverTimestamp()
      });
  }
};

/*
 * mutations
 */

export const mutations: MutationTree<ModuleState> = {
  SAVE_TAGS(state, props: { tags: string[] }) {
    const tags = Object.assign([], props.tags);
    Vue.set(state, "tags", tags);
  },

  SAVE_FILES(state, props: { files: File[] }) {
    const files = Object.assign([], props.files);
    Vue.set(state, "files", files);
  },

  REMOVE_FILE(state, props: { index: number }) {
    state.files.splice(props.index, 1);
  },

  REMOVE_ALL_FILES(state, props: { index: number }) {
    Vue.set(state, "files", []);
  },

  UPLOADING(state, props: { uploading: boolean }) {
    state.uploading = props.uploading;
  }
};
