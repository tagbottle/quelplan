import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
const localVue = createLocalVue();
localVue.use(Vuex);

// @ts-ignore
import Component from "../images.vue";
import * as UploaderStore from "~/store/searcher";
import "~/plugins/buefy";

describe("images.vue", () => {
  let store;
  let searcherStore;
  let imageUrls: string[] = [];

  beforeEach(() => {
    // generate uploader store
    searcherStore = {
      ...UploaderStore,
      namespaced: true
    };

    // set getters mock
    searcherStore.getters["imageUrls"] = jest.fn(function() {
      return imageUrls;
    });
    imageUrls = [
      "https://example.com/image1.jpg",
      "https://example.com/image2.jpg"
    ];

    // generate vuex store
    store = new Vuex.Store({
      modules: { searcher: searcherStore }
    });
  });

  it("should get image urls from vuex getters", async () => {
    const wrapper = mount(Component, { store, localVue });
    expect(searcherStore.getters["imageUrls"]).toBeCalled();
  });

  it("should use image urls as `src` of image tagss", async () => {
    const wrapper = mount(Component, { store, localVue });

    // test with multiple image urls
    expect(imageUrls.length).toBeGreaterThan(1);

    for (const key in imageUrls) {
      // get element for specific url
      const index: number = parseInt(key);
      const url = imageUrls[index];
      const selector = `figure:nth-child(${index + 1}) img`;

      // img tag should contain the correct `src` attribute
      const element = wrapper.find(selector);
      expect(element.attributes("src")).toEqual(url);
    }
  });
});
