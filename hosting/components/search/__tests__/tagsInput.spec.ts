import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
const localVue = createLocalVue();
localVue.use(Vuex);

// @ts-ignore
import Component from "../tagsInput.vue";
import * as SearcherStore from "~/store/searcher";
import "~/plugins/buefy";

describe("tagsInput.vue", () => {
  let store;
  let searcherStore;

  beforeEach(() => {
    // generate uploader store
    searcherStore = {
      ...SearcherStore,
      namespaced: true
    };

    // set action mock
    searcherStore.actions.updateTags = jest.fn();

    // generate vuex store
    store = new Vuex.Store({
      modules: { searcher: searcherStore }
    });
  });

  it("should have `tags` value", () => {
    const wrapper = mount(Component, { store, localVue });
    expect(wrapper.vm).toHaveProperty("tags", []);
  });

  it("dipatches an aciton to update tag", () => {
    const wrapper = mount(Component, { store, localVue });
    const tags = ["a", "b", "c"];
    (wrapper.vm as Component).updateTags(tags);
    expect(searcherStore.actions.updateTags).toHaveBeenCalled();
    expect(searcherStore.actions.updateTags.mock.calls[0][1]).toEqual({ tags });
  });
});
