import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
const localVue = createLocalVue();
localVue.use(Vuex);

// @ts-ignore
import Component from "../draggableUploadBox.vue";
import * as UploaderStore from "~/store/uploader";
import "~/plugins/buefy";

describe("draggableUploadBox.vue", () => {
  let store;
  let uploaderStore;

  beforeEach(() => {
    // generate uploader store
    uploaderStore = {
      ...UploaderStore,
      namespaced: true
    };

    // set action mock
    uploaderStore.actions.updateFiles = jest.fn();
    uploaderStore.actions.removeFile = jest.fn();

    // generate vuex store
    store = new Vuex.Store({
      modules: { uploader: uploaderStore }
    });
  });

  it("should have `files` value", () => {
    const wrapper = mount(Component, { store, localVue });
    expect(wrapper.vm).toHaveProperty("files", []);
  });

  it("dipatches an aciton to update files", () => {
    const wrapper = mount(Component, { store, localVue });
    const files = [new File([], "test file")];
    (wrapper.vm as Component).uploadFiles(files);
    expect(uploaderStore.actions.updateFiles).toHaveBeenCalled();
    expect(uploaderStore.actions.updateFiles.mock.calls[0][1]).toEqual({
      files
    });
  });

  it("dipatches an aciton to delete file", () => {
    // 1. add file
    const wrapper = mount(Component, { store, localVue });
    const vm = wrapper.vm as Component;
    const files = [new File([], "test file")];
    vm.uploadFiles(files);

    // 2. remove it
    const index = 0;
    vm.removeFile(index);

    // test
    expect(uploaderStore.actions.removeFile).toHaveBeenCalled();
    expect(uploaderStore.actions.removeFile.mock.calls[0][1]).toEqual({
      index
    });
    expect(vm.files).toEqual([]);
  });
});
