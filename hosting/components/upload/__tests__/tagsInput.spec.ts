import { mount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
const localVue = createLocalVue();
localVue.use(Vuex);

// @ts-ignore
import Component from "../tagsInput.vue";
import * as UploaderStore from "~/store/uploader";
import "~/plugins/buefy";

describe("tagsInput.vue", () => {
  let store;
  let uploaderStore;

  beforeEach(() => {
    // generate uploader store
    uploaderStore = {
      ...UploaderStore,
      namespaced: true
    };

    // set action mock
    uploaderStore.actions.updateTags = jest.fn();

    // generate vuex store
    store = new Vuex.Store({
      modules: { uploader: uploaderStore }
    });
  });

  it("should have `tags` value", () => {
    const wrapper = mount(Component, { store, localVue });
    expect(wrapper.vm).toHaveProperty("tags", []);
  });

  it("dipatches an aciton to update tag", () => {
    const wrapper = mount(Component, { store, localVue });
    const tags = ["a", "b", "c"];
    (wrapper.vm as Component).updateTags(tags);
    expect(uploaderStore.actions.updateTags).toHaveBeenCalled();
    expect(uploaderStore.actions.updateTags.mock.calls[0][1]).toEqual({ tags });
  });
});
