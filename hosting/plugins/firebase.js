import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import configs from "./firebaseConfig.json";

export default async ({ store }, inject) => {
  // init
  if (!firebase.apps.length) {
    firebase.initializeApp(configs[process.env.deployTarget].config);
    firebase.firestore().settings({
      timestampsInSnapshots: true
    });
  }

  // auth
  if (process.client) {
    firebase.auth().onAuthStateChanged(
      async user => {
        store.commit("auth/INITIALIZED");
        if (user) {
          store.commit("auth/LOGIN");
        } else {
          store.commit("auth/LOGOUT");
        }
      },
      error => {
        console.error("firebase.auth().onAuthStateChanged error", error);
        if (process.client) {
          alert(error);
        }
        store.commit("auth/LOGOUT");
      }
    );
  }

  inject("firebase", firebase);
};
