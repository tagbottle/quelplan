declare namespace NodeJS {
  interface Process {
    client: boolean;
    browser: boolean;
    deployTarget: "default" | "alpha";
  }
}
