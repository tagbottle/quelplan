const parseArgs = require("minimist");
const argv = parseArgs(process.argv.slice(2), {
  alias: {
    H: "hostname",
    p: "port"
  },
  string: ["H"],
  unknown: () => false
});

const port =
  argv.port ||
  process.env.PORT ||
  process.env.npm_package_config_nuxt_port ||
  "3000";
const host =
  argv.hostname ||
  process.env.HOST ||
  process.env.npm_package_config_nuxt_host ||
  "localhost";

// deploy target
function deployTarget(target) {
  return "prod";
}

module.exports = {
  env: {
    baseUrl: process.env.BASE_URL || `http://${host}:${port}`,
    deployTarget: deployTarget(process.env.DEPLOY_TARGET)
  },
  head: {
    title: "quelplan",
    titleTemplate: "%s | quelplan",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css"
      }
    ],
    script: []
  },

  /*
  ** Build configuration
  */

  build: {
    publicPath: "/assets/",
    extractCSS: true,
    babel: {
      plugins: [
        [
          "@babel/plugin-transform-runtime",
          {
            corejs: 2,
            regenerator: true
          }
        ]
      ]
    },
    postcss: {
      plugins: {
        // to stop warning with bulma https://github.com/nuxt/nuxt.js/issues/1670
        "postcss-custom-properties": false
      }
    },
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.pug$/,
        loader: "pug-plain-loader",
        options: {
          data: {}
        }
      });
    }
  },

  plugins: [
    "~/plugins/buefy.js",
    "~/plugins/firebase.js"
  ],

  modules: [
    "~/modules/typescript.js",
    "@nuxtjs/pwa"
  ],

  // pwa module
  manifest: {
    name: "quelplan",
    lang: "ja",
    gcm_sender_id: "103953800507"
  }
};
