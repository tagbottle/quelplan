// https://soumak77.github.io/firebase-mock/tutorials/integration/jest.html

import firebaseMock from "firebase-mock";

const mockauth = new firebaseMock.MockAuthentication();
const mockdatabase = new firebaseMock.MockFirebase();
const mockfirestore = new firebaseMock.MockFirestore();
const mockstorage = new firebaseMock.MockStorage();
const mockmessaging = new firebaseMock.MockMessaging();

export const firebase = new firebaseMock.MockFirebaseSdk(
  // use null if your code does not use RTDB
  path => {
    return path ? mockdatabase.child(path) : mockdatabase;
  },
  // use null if your code does not use AUTHENTICATION
  () => {
    return mockauth;
  },
  // use null if your code does not use FIRESTORE
  () => {
    return mockfirestore;
  },
  // use null if your code does not use STORAGE
  () => {
    return mockstorage;
  },
  // use null if your code does not use MESSAGING
  () => {
    return mockmessaging;
  }
);

module.exports = firebase;
