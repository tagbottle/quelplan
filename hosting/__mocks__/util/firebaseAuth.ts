import firebase from "firebase/app";
import FirebaseAuthMock from "firebase-mock/src/firebase-auth";

export const login = (): void => {
  const authMock = firebase.auth() as FirebaseAuthMock;
  authMock.changeAuthState({
    uid: "testUid",
    provider: "test-mock",
    token: "authToken",
    expires: Math.floor(Date.now() / 1000) + 24 * 60 * 60
  });
  authMock.flush();
};

export const logout = (): void => {
  const authMock = firebase.auth() as FirebaseAuthMock;
  authMock.changeAuthState(null);
  authMock.flush();
};
