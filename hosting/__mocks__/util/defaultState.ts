import * as States from "~/store/index";
import { state as defaultAuthState } from "~/store/auth";
import { state as defaultUploaderState } from "~/store/uploader";
import { state as defaultSearcherState } from "~/store/searcher";

const defaultState = (): States.IState => {
  return {
    auth: defaultAuthState(),
    uploader: defaultUploaderState(),
    searcher: defaultSearcherState()
  };
};

export {
  defaultState,
  defaultAuthState,
  defaultUploaderState,
  defaultSearcherState
};

export default defaultState;
